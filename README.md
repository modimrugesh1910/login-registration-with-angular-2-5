# angular2/5-registration-login-example

This is a demo project for angular2/5 registration & login.

## Dev Build

1. `npm install`
2. `npm start`

## Prod Build

1. `npm run build`

## Contact

modimrugesh1910@gmail.com

